import time

message = "Hello world !"
output = ["" for i in range(len(message))]
i = 0
while "".join(output) != message:
    if output[i] == message[i]:
        i += 1
    if output[i] == "":
        output[i] = chr(32)
    else:
        output[i] = chr(ord(output[i]) + 1)
    print("".join(output))
    time.sleep(0.01)
